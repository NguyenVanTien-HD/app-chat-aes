const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const crypto = require('crypto');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const router = require('./router');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(cors());
app.use(router);

io.on('connect', (socket) => {
  socket.on('join', ({ name, room, publicKey }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if(error) return callback(error);

    console.log('publicKey1',publicKey);

    // Check partner exist
    const users = getUsersInRoom(room);
    console.log('users',users);
    if (users.length > 1) {
      const partner = users.filter(u => u.name !== name);
      // Trao doi khoa
      socket.broadcast.to(user.room).emit('publicKey', {user: user, room: room, publicKey: publicKey});
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
    }

      socket.join(user.room);

      socket.emit('message', { user: 'admin', text: `${user.name}, chào mừng bạn đã vào phòng ${user.room}. Tin nhắn của bạn sẽ được mã hóa!`});
      socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} đã vào!` });
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });


    callback();
  });

  socket.on('rePublicKey', ({user, publicKey}, callback) => {
    socket.join(user.room);
    console.log('publicKey2',publicKey);

    socket.broadcast.to(user.room).emit('finalPublicKey', { publicKey });

  });

  socket.on('sendMessage', ({ message, hash }, callback) => {
    const user = getUser(socket.id);
    console.log('message',message)
    // const msg = message.substring(0, message.length - 2) + "d";
    // const msg = message + "d";
    const msg = message;
    io.to(user.room).emit('message', { user: user.name, text: msg, hash: hash });

    callback();
  });

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);

    if(user) {
      io.to(user.room).emit('message', { user: 'admin', text: `${user.name} đã rời đi.` });
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
    }
  })
});

server.listen(process.env.PORT || 5000, () => console.log(`Server has started.`));
