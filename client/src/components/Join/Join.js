import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import './Join.css';

export default function SignIn() {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');

  const sendInfoToJoin = (event) => {
    if (!name || !room) {
      return event.preventDefault();
    } return null;
  };

  return (
    <div className="joinOuterContainer">
      <div className="joinInnerContainer">
        <h1 className="heading">Tham gia chat</h1>
        <div>
          <input placeholder="Tên đăng nhâp" className="joinInput" type="text" onChange={(event) => setName(event.target.value)} />
        </div>
        <div>
          <input placeholder="Phòng chat" className="joinInput mt-20" type="text" onChange={(event) => setRoom(event.target.value)} />
        </div>
        <Link onClick={sendInfoToJoin} to={`/chat?name=${name}&room=${room}`}>
          <button className="button mt-20" type="submit">Đăng nhập</button>
        </Link>
      </div>
    </div>
  );
}
