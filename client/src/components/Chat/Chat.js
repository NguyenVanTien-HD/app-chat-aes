import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';

import TextContainer from '../TextContainer/TextContainer';
import Messages from '../Messages/Messages';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';

import './Chat.css';

const crypto = require('crypto');

const CryptoJS = require('crypto-js');

// const ENDPOINT = 'http://192.168.137.46:5000';
const ENDPOINT = 'localhost:5000';

// const key = '123456';

let socket;

// eslint-disable-next-line react/prop-types
const Chat = ({ location }) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [users, setUsers] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  const [privateKey, setPrivateKey] = useState('');

  let secret;

  let publicKey;
  let alice;

  useEffect(() => {
    // eslint-disable-next-line no-shadow
    const { name, room } = queryString.parse(location.search);
    document.title = 'Phòng chat';

    socket = io(ENDPOINT);
    setRoom(room);
    setName(name);

    alice = crypto.createECDH('secp256k1');
    alice.generateKeys();
    publicKey = alice.getPublicKey().toString('hex');

    socket.emit('join', { name, room, publicKey }, (error) => {
      if (error) {
        alert(error);
      }
    });
    // eslint-disable-next-line react/prop-types
  }, [ENDPOINT, location.search]);

  useEffect(() => {
    socket.on('publicKey', (obj) => {
      console.log('bob key ..', obj);
      const bob = crypto.createECDH('secp256k1');
      bob.generateKeys();
      publicKey = bob.getPublicKey().toString('hex');
      secret = Buffer.from(bob.computeSecret(
        Buffer.from(obj.publicKey, 'hex'),
      )).toString('hex');

      setPrivateKey(secret);

      console.log('publickey alice', publicKey);
      console.log('khoa chung 1', secret);
      socket.emit('rePublicKey', { user: obj.user, publicKey });
    });
  }, []);

  useEffect(() => {
    socket.on('finalPublicKey', (obj) => {
      console.log('publickey alice', obj);

      secret = Buffer.from(
        alice.computeSecret(
          Buffer.from(obj.publicKey, 'hex'),
        ),
      ).toString('hex');

      setPrivateKey(secret);

      console.log('khoa chung 2', secret);
    });
  }, []);

  useEffect(() => {
    socket.on('message', (obj) => {
      const res = { ...obj };
      console.log(obj);
      console.log('giai ma', secret);
      if (obj.user !== 'admin') {
        // Bam msgEncrypt + secret
        const msgToHash = secret + obj.text;
        const hashValue = crypto
          .createHash('sha256')
          .update(msgToHash)
          .digest('hex');
        console.log(hashValue);
        if (hashValue === obj.hash) {

          // Decrypt
          const bytes = CryptoJS.AES.decrypt(obj.text, secret);
          console.log(bytes);
          res.text = JSON.parse(
            bytes.toString(CryptoJS.enc.Utf8),
          );
        } else {
          res.text = 'Tin nhắn đã bị sửa đổi';
        }
      }
      setMessages((msg) => [...msg, res]);
    });

    // eslint-disable-next-line no-shadow
    socket.on('roomData', ({ users }) => {
      setUsers(users);
    });
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();
    if (message) {
      // Encode here

      console.log('privateKey', privateKey);

      const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify(message),
        privateKey,
      ).toString();
      const msgAndKey = privateKey + ciphertext;
      const hashValue = crypto
        .createHash('sha256')
        .update(msgAndKey)
        .digest('hex');
      console.log(ciphertext);

      socket.emit('sendMessage', { message: ciphertext, hash: hashValue }, () => setMessage(''));
    }
  };

  // eslint-disable-next-line no-unused-vars
  const test = () => {
    // eslint-disable-next-line no-shadow
    const alice = crypto.getDiffieHellman('modp15');
    const bob = crypto.getDiffieHellman('modp15');
    alice.generateKeys();
    bob.generateKeys();

    console.log(bob.getPublicKey());
    console.log(alice.getPublicKey());
    // console.log(publicKeyBob);

    // // Exchange and generate the secret...
    const aliceSecret = Buffer.from(
      alice.computeSecret(
        Buffer.from(bob.getPublicKey().toString('hex'), 'hex'),
      ),
    ).toString('hex');
    const bobSecret = Buffer.from(bob.computeSecret(
      // alice.getPublicKey().toString('hex')
      Buffer.from(alice.getPublicKey().toString('hex'), 'hex'),
    )).toString('hex');

    console.log(bobSecret === aliceSecret);
  };

  // eslint-disable-next-line no-unused-vars
  const test2 = () => {
    const bob = crypto.getDiffieHellman('modp15');
    bob.generateKeys();

    // publicKeyBob = (bob.getPublicKey().toString('hex'));

    // Exchange and generate the secret...
    // const aliceSecret = Buffer.from(alice.computeSecret(bob.getPublicKey())).toString('hex');
    // const bobSecret = Buffer.from(bob.computeSecret(alice.getPublicKey())).toString('hex');
    //
    // console.log(bobSecret === aliceSecret)
  };

  const testSHA = () => {
    const hash = crypto.createHash('sha256');

    const code = 'bacon';
    const bamMa = crypto.createHash('sha256').update(code).digest('hex');

    const code2 = 'bacon';
    const bamMa2 = crypto.createHash('sha256').update(code2).digest('hex');
    console.log(bamMa)
    console.log(bamMa2)
    // code = hash.update(code);
    // console.log('1111', code);
    // code = hash.digest(code);
    // console.log('2222', code);

  };

  return (
    <div className="outerContainer">
      <div className="container">
        <InfoBar room={room} />
        <Messages messages={messages} name={name} />
        <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
      </div>
      <TextContainer users={users} />
    </div>
  );
};

export default Chat;
